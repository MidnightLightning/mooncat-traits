# MoonCat Traits parsing

[MoonCats](https://mooncatrescue.com/) are a project that was launched on the [Ethereum](https://ethereum.org/) blockchain in 2017 as a unique "crypto art" project, whereby 25,600 pixelated cats came into being.

The appearance of each of the MoonCats is determined by their base ID number, and since IDs were determined very randomly, there's a wide range of MoonCat appearances. This repository is meant to illustrate different ways to categorize the MoonCats, and see how large those "buckets" are.

Do note, these different "traits" show how _rare_ something is, but that doesn't necessarily equate to how _valuable_ that trait is; just because only one MoonCat is in a category of its own when sliced a certain way, doesn't directly mean _all_ buyers would then pay top-dollar for it.

# Output files
This repository stores several files that may be of use to different people for different reasons:

- `Cats.json`: A JSON-formatted list of data about each cat. This is the source data that's parsed to assign traits, but other projects may find this base data useful too.
- `build/traits.csv`: An output file of all cats and all traits. This is structured as a CSV with three columns, linking a cat with a property, and that cat's value.
- `build/traits.html`: An output file of cat traits intended to be more human-readable than the CSV output. This shows all the different cat traits and different metrics about them. If a specific trait has less than 1,500 cats with that trait, the actual cat IDs having that trait are listed out.
- `build/colors.html`: An output file showing cats grouped by colors, a more human-readable format than CSV. This file shows all the cats grouped with other cats that share the same rendered colors as them.

# Contributing
This project was born out of my own random scripts to figure out "how many MoonCats are X" questions the community started asking in March 2021 when the project was "rediscovered". These are not "official", and are seeded with mainly what attributes/aspects I consider noteworthy. If you have other aspects you think should be included and tracked here as ones that would be noteworthy to others, feel free to contribute!

If you're a developer, feel free to open a Merge Request with suggested changes. If you're not a developer, create a new "Issue" for this project, describing what aspect you think is noteworthy, and how it's defined (e.g. "I think all MoonCats matching this particular shade of yellow should be tagged as 'Big Birds'."). The community can discuss/comment on those, and if there's consensus, a developer can pick it up and write the code for it.