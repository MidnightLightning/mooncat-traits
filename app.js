const catData = require('./Cats.json');
const fs = require('fs');
const { derivePalette } = require('./colorUtils');

const totalCats = catData.length;
const poseNames = {
  0: 'Standing',
  1: 'Sleeping',
  2: 'Pouncing',
  3: 'Stalking'
};
const patternNames = {
  0: 'Solid',
  1: 'Striped',
  2: 'Eyepatch',
  3: 'Half/Half'
};
const expressionNames = {
  0: 'Smile',
  1: 'Frown (look down)',
  2: 'Frown (look up)',
  3: 'Flat whiskers'
};
const searchStrings = [
  '0x007', '000', '0000', '00000',
  '0123', '01234', '012345',
  '123', '1234', '12345', '123456',
  '234', '2345', '23456', '234567',
  '345', '3456', '34567', '345678',
  '456', '4567', '45678', '456789',
  '567', '5678', '56789', '567890',
  '678', '6789', '67890',
  '789', '8790',
  'b00b', 'beef', 'dead', 'aced',
  'b000b', 'b0000b',
  'fff', 'ffff', 'ffffff',
  'ff00ff'
];
const colorBuckets = {};
const hueRanges = {
  15: 'Red',
  45: 'Orange',
  75: 'Yellow',
  105: 'Chartreuse',
  135: 'Green',
  165: 'Teal',
  195: 'Cyan',
  225: 'Sky Blue',
  255: 'Blue',
  285: 'Purple',
  315: 'Magenta',
  345: 'Fuchsia'
}
function getColorName(hue) {
  let maxValues = Object.keys(hueRanges).sort((a, b) => a - b);
  for (let i = 0; i < maxValues.length; i++) {
    if (hue < maxValues[i]) {
      return hueRanges[maxValues[i]];
    }
  }
  return 'Red';
}

/**
 * Hashing function, to give each color palette a unique identifier.
 */
function getPaletteId(palette) {
  let toHash = JSON.stringify(palette);
  let hash = 0, i, chr;
  if (toHash.length === 0) return hash;
  for (i = 0; i < toHash.length; i++) {
    chr   = toHash.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

const traitData = {};
function initTrait(key, label, value = '', details = '') {
  if (typeof traitData[key] !== 'undefined') return;
  traitData[key] = {
    label: label,
    value: value,
    details: details,
    cats: []
  }
};
initTrait('facing:1', 'Facing', 'Right');
initTrait('facing:0', 'Facing', 'Left');
Object.keys(hueRanges).forEach(hueMax => {
  let colorName = hueRanges[hueMax];
  initTrait(`color:${colorName.toLowerCase()}`, 'Color', colorName)
});
Object.keys(poseNames).forEach(key => {
  initTrait(`pose:${key}`, 'Pose', poseNames[key]);
});
Object.keys(patternNames).forEach(key => {
  initTrait(`pattern:${key}`, 'Pattern', patternNames[key]);
});
Object.keys(expressionNames).forEach(key => {
  initTrait(`expression:${key}`, 'Expression', expressionNames[key]);
});
initTrait('inverted', 'Inverted Color Palette');
searchStrings.forEach(searchString => {
  let key = searchString.toLowerCase();
  initTrait(`search:${key}`, 'ID pattern', searchString);
});
initTrait('genesis', 'Genesis');
initTrait('ponderware-gift', 'Ponderware Gift');
initTrait('leadingZeroes:3', 'Leading Zeroes', '3', 'ID starts with triple-zeroes');
initTrait('leadingZeroes:4', 'Leading Zeroes', '4', 'ID starts with quad-zeroes');
initTrait('leadingZeroes:5', 'Leading Zeroes', '5', 'ID starts with quint-zeroes');
initTrait('all-letters', 'All Letters', '', 'ID contains all letters (excluding first byte)');
initTrait('all-numbers', 'All Numbers', '', 'ID contains all numbers');
initTrait('bond', 'James Bond', '', 'ID starts with "0x007"');

initTrait('full-red', 'Red Channel', 'Max', 'Red color channel is 255 (maximum)');
initTrait('no-red', 'Red Channel', 'Min', 'Red color channel is zero (minimum)');
initTrait('full-green', 'Green Channel', 'Max', 'Green color channel is 255 (maximum)');
initTrait('no-green', 'Green Channel', 'Min', 'Green color channel is zero (minimum)');
initTrait('full-blue', 'Blue Channel', 'Max', 'Blue color channel is 255 (maximum)');
initTrait('no-blue', 'Blue Channel', 'Min', 'Blue color channel is zero (minimum)');

initTrait('mint:500', 'Round Mint Number', '', 'Mint order is a multiple of 500');
initTrait('calendar-first:2017', 'First Rescue in Calendar Year', '2017');
initTrait('calendar-last:2017', 'Last Rescue in Calendar Year', '2017');
initTrait('calendar-first:2018', 'First Rescue in Calendar Year', '2018');
initTrait('calendar-last:2018', 'Last Rescue in Calendar Year', '2018');
initTrait('calendar-first:2019', 'First Rescue in Calendar Year', '2019');
initTrait('calendar-last:2019', 'Last Rescue in Calendar Year', '2019');
initTrait('calendar-first:2020', 'First Rescue in Calendar Year', '2020');
initTrait('calendar-last:2020', 'Last Rescue in Calendar Year', '2020');
initTrait('calendar-first:2021', 'First Rescue in Calendar Year', '2021');
initTrait('calendar-last:2021', 'Last Rescue in Calendar Year', '2021');
initTrait('calendar-year:2017', 'Rescued in Calendar Year', '2017');
initTrait('calendar-year:2018', 'Rescued in Calendar Year', '2018');
initTrait('calendar-year:2019', 'Rescued in Calendar Year', '2019');
initTrait('calendar-year:2020', 'Rescued in Calendar Year', '2020');
initTrait('calendar-year:2021', 'Rescued in Calendar Year', '2021');

// Parse the cats, and add traits
function addTrait(catId, traitKey, details = '') {
  if (typeof traitData[traitKey] == 'undefined') {
    throw new Error(`Trait "${traitKey}" is not initialized`);
  }
  traitData[traitKey].cats.push({ id: catId, details: details });
}
catData.forEach(cat => {
  addTrait(cat.id, `facing:${cat.facing}`);
  addTrait(cat.id, `pose:${cat.pose}`);
  addTrait(cat.id, `pattern:${cat.fur}`);
  addTrait(cat.id, `expression:${cat.face}`);

  if (cat.id.substr(0, 4) == '0xff') {
    addTrait(cat.id, 'genesis');
  }
  if (cat.id.substr(0, 5) == '0x000') {
    addTrait(cat.id, 'leadingZeroes:3');
  }
  if (cat.id.substr(0, 5) == '0x007') {
    addTrait(cat.id, 'bond');
  }
  if (cat.id.substr(0, 6) == '0x0000') {
    addTrait(cat.id, 'leadingZeroes:4');
  }
  if (cat.id.substr(0, 7) == '0x00000') {
    addTrait(cat.id, 'leadingZeroes:5');
  }

  searchStrings.forEach(searchString => {
    if (cat.id.indexOf(searchString) >= 0) {
      addTrait(cat.id, `search:${searchString.toLowerCase()}`);
    }
  });

  // See if ID is all numbers or all letters (excluding first byte)
  if (/^[a-z]+$/i.test(cat.id.substr(4, 10))) {
    addTrait(cat.id, 'all-letters');
  }
  if (/^[0-9]+$/.test(cat.id.substr(4,10))) {
    addTrait(cat.id, 'all-numbers');
  }

  // Mint order notable values
  if (cat.mint % 500 == 0) {
    addTrait(cat.id, 'mint:500');
  }
  if (cat.mint == 0) {
    addTrait(cat.id, 'calendar-first:2017');
  } else if (cat.mint == 3364) {
    addTrait(cat.id, 'calendar-last:2017');
  } else if (cat.mint == 3365) {
    addTrait(cat.id, 'calendar-first:2018');
  } else if (cat.mint == 5683) {
    addTrait(cat.id, 'calendar-last:2018');
  } else if (cat.mint == 5684) {
    addTrait(cat.id, 'calendar-first:2019');
  } else if (cat.mint == 5754) {
    addTrait(cat.id, 'calendar-last:2019');
  } else if (cat.mint == 5755) {
    addTrait(cat.id, 'calendar-first:2020');
  } else if (cat.mint == 5757) {
    addTrait(cat.id, 'calendar-last:2020');
  } else if (cat.mint == 5758) {
    addTrait(cat.id, 'calendar-first:2021');
  } else if (cat.mint == 25439) {
    addTrait(cat.id, 'calendar-last:2021');
  }
  if (cat.mint <= 3364) {
    addTrait(cat.id, 'calendar-year:2017');
  } else if (cat.mint <= 5683) {
    addTrait(cat.id, 'calendar-year:2018');
  } else if (cat.mint <= 5754) {
    addTrait(cat.id, 'calendar-year:2019');
  } else if (cat.mint <= 5757) {
    addTrait(cat.id, 'calendar-year:2020');
  } else {
    addTrait(cat.id, 'calendar-year:2021');
  }

  [
    '86', // https://mooncatrescue.com/vote/airdrop
    '100',
    '263',
    '291',
    '590',
    '908',
    '972',
    '6' // https://twitter.com/ponderware/status/1371651192930119684
  ].forEach(mintOrder => {
    if (cat.mint == mintOrder) {
      addTrait(cat.id, 'ponderware-gift');
    }
  })

  if (cat.id.substr(0, 4) !== '0xff') {
    // Non-Genesis traits
    if (cat.inverted == 1) addTrait(cat.id, `inverted`);

    let hueName = getColorName(cat.hsl.h).toLowerCase();
    addTrait(cat.id, `color:${hueName}`);

    if (cat.rgb.r == 255) {
      addTrait(cat.id, 'full-red');
    } else if (cat.rgb.r == 0) {
      addTrait(cat.id, 'no-red');
    }
    if (cat.rgb.g == 255) {
      addTrait(cat.id, 'full-green');
    } else if (cat.rgb.g == 0) {
      addTrait(cat.id, 'no-green');
    }
    if (cat.rgb.b == 255) {
      addTrait(cat.id, 'full-blue');
    } else if (cat.rgb.b == 0) {
      addTrait(cat.id, 'no-blue');
    }

    // Add to rendered-color bucket (for twin-parsing later)
    let palette = derivePalette(cat.rgb.r, cat.rgb.g, cat.rgb.b, cat.inverted);
    let paletteId = getPaletteId(palette);
    if (typeof colorBuckets[paletteId] == 'undefined') {
      colorBuckets[paletteId] = {
        palette: palette,
        hue: cat.hsl.h,
        inverted: cat.inverted,
        cats: []
      };
    }
    colorBuckets[paletteId].cats.push(cat);
  }
});

// Initialize final output files
let htmlTemplate = [];
htmlTemplate.push('<html><head><title>MoonCat Traits</title>');
htmlTemplate.push('<style type="text/css">');
htmlTemplate.push(`
.color-chip { display: inline-block; min-width: 20px; }
.cat-id { font-family: monospace; color:#900; font-size: 0.9rem; }
.cat-list { display: flex; flex-wrap: wrap }
.cat-card {
  border:solid 1px #CCC;
  padding: 1rem; margin: 0.5rem;
  display:flex; flex-direction: column;
  justify-content: flex-end; align-items: center;
}
body { position: relative; }
#tooltip { background-color: #FFF; padding: 1rem; border:solid 1px #CCC; position: absolute; display: none; }
`);
htmlTemplate.push('</style>');
htmlTemplate.push('<script type="text/javascript" src="https://bafybeiagdr2smk7fh2xnhsnrlptwcjleztgv2rydpxgeuzfpzpw7mu5zsi.ipfs.dweb.link/"></script>');
htmlTemplate.push('<script type="text/javascript">');
htmlTemplate.push(`$(document).ready(() => {
  let $tooltip = $('#tooltip');
  $('body')
    .on('mouseover', '.cat-id', e => {
      let catId = e.target.textContent;
      let k = catId.substr(4, 2);
      let imgSrc = 'https://bafybeihc3a4k4ybjduese6ipsxqg4rvlissts7obtuajffj4nmgraizqcy.ipfs.dweb.link/' + k + '/' + catId + '.svg';
      let pos = $(e.target).position();
      let tooltipBottom = $(document).height() - pos.top - 20;
      $tooltip
        .html('<img src="' + imgSrc + '" />')
        .css('bottom', tooltipBottom)
        .css('left', pos.left)
        .show();
    })
    .on('mouseout', '.cat-id', e => {
      $tooltip.hide();
    })
    .on('click', e => {
      $tooltip.hide();
    });
});`)
htmlTemplate.push('</script>');

// Main HTML output file
let html = JSON.parse(JSON.stringify(htmlTemplate));
html.push('</head><body><h1>MoonCat Traits</h1>');

// Color groupings output file
let colorHtml = JSON.parse(JSON.stringify(htmlTemplate));
colorHtml.push('</head><body><h1>MoonCat Color Groups</h1>');
colorHtml.push('<p>This file groups cats together with those that are rendered using the same color palette. Each cat has a specific RGB value that\'s part of their ID, but when rendered, that color becomes the "fully saturated" version of that color in HSL colorspace. Due to that, many different RGB values end up being rendered with the same color palette. Hence you will see below that cats in the same grouping don\'t have ID values showing RGB values that match, but if you look at the rendered version of the cats, they\'re the same color.</p>');

let csv = [];
csv.push('"Cat ID",Trait,Value');

function showCatList(cats) {
  if (cats.length > 1500) return '';

  let catListing = cats.map(cat => {
    return `<span class="cat-id">${cat.id}</span>`;
  }).join(', ');
  return `<p>${catListing}</p>`;
}

Object.keys(traitData).forEach(traitKey => {
  // Add CSV data
  traitData[traitKey].cats.forEach(cat => {
    csv.push(`${cat.id},"${traitData[traitKey].label}","${traitData[traitKey].value}"`);
  });

  // Add HTML data
  let catCount = traitData[traitKey].cats.length;
  if (catCount > 0) {
    let percentage = (catCount / totalCats) * 100;
    if (percentage >= 1) {
      percentage = percentage.toFixed(2);
    } else if (percentage >= 0.1) {
      percentage = percentage.toFixed(3);
    } else if (percentage >= 0.01) {
      percentage = percentage.toFixed(4);
    } else if (percentage >= 0.001) {
      percentage = percentage.toFixed(5);
    }
    let valueTag = (traitData[traitKey].value != '') ? ': ' + traitData[traitKey].value : '';
    html.push(`<h2>${traitData[traitKey].label}${valueTag}</h2>`);
    let details = '';
    if (traitData[traitKey].details != '') {
      details = traitData[traitKey].details + '. ';
    }
    html.push(`<p>${details}A total of ${catCount.toLocaleString()} MoonCats (${percentage}%) have this trait</p>`);
    html.push(showCatList(traitData[traitKey].cats));
  }
});

// Parse color buckets

// Find cats that have a unique color
let uniqueColors = [];
Object.keys(colorBuckets).filter(colorKey => {
  return colorBuckets[colorKey].cats.length == 1;
}).forEach(colorKey => {
  uniqueColors.push(colorBuckets[colorKey].cats[0]);
});
if (uniqueColors.length > 0) {
  uniqueColors.forEach(cat => {
    csv.push(`${cat.id},"Unique Color"`);
  });

  let catCount = uniqueColors.length;
  let percentage = ((catCount / totalCats) * 100).toFixed(2);

  html.push(`<h2>Unique Color</h2>`);
  html.push(`<p>Cats who when rendered, use a five-color palette that no other cat in the set uses. A total of ${catCount} MoonCats (${percentage}%) are in this set.</p>`);
  html.push(showCatList(uniqueColors));
}

Object.keys(colorBuckets).sort((a, b) => {
  return colorBuckets[a].hue - colorBuckets[b].hue;
}).forEach(colorKey => {
  if (colorBuckets[colorKey].cats.length == 1) return;

  let colorCode = colorBuckets[colorKey].palette[3]; // The color most of the cat's body is colored with
  let paletteTag = colorCode + ':' + colorKey;

  // Add overall CSV data
  colorBuckets[colorKey].cats.forEach(cat => {
    csv.push(`${cat.id},"Render Color","${paletteTag}"`);
  });

  // Add overall HTML data
  let catCount = colorBuckets[colorKey].cats.length;
  colorHtml.push(`<h2>Render Color: ${paletteTag}</h2>`);
  colorHtml.push(`<p>Cats who, when rendered, all have a primary color of <tt>${colorCode}</tt> <span class="color-chip" style="background-color:${colorCode}">&nbsp;</span>. A total of ${catCount} cats are in this set.</p>`);
  colorHtml.push(showCatList(colorBuckets[colorKey].cats));

  // Find more strict matches
  let twins = {};
  let doppelgangers = {};
  colorBuckets[colorKey].cats.forEach(cat => {
    let twinKey = cat.fur + ':' + cat.face;
    if (typeof twins[twinKey] == 'undefined') {
      twins[twinKey] = [];
    }
    twins[twinKey].push(cat);

    let doppelgangerKey = cat.fur + ':' + cat.face + ':' + cat.pose;
    if (typeof doppelgangers[doppelgangerKey] == 'undefined') {
      doppelgangers[doppelgangerKey] = [];
    }
    doppelgangers[doppelgangerKey].push(cat);
  });

  Object.keys(twins).forEach(twinKey => {
    if (twins[twinKey].length == 1) return;
    let details = [
      paletteTag,
      patternNames[twins[twinKey][0].fur],
      expressionNames[twins[twinKey][0].face]
    ];

    // Add CSV data
    twins[twinKey].forEach(cat => {
      csv.push(`${cat.id},"Identical Twin","${details.join(',')}"`);
    });

    // Add HTML data
    let catCount = twins[twinKey].length;
    colorHtml.push(`<h2>Identical Twins: ${details.join(', ')}</h2>`);
    colorHtml.push(`<p>Cats who, when rendered, all have the same color palette, focused on a main color of <tt>${colorCode}</tt> <span class="color-chip" style="background-color:${colorCode}">&nbsp;</span> and have the same fur pattern and facial expression. A total of ${catCount} cats are in this set.</p>`);
    colorHtml.push(showCatList(twins[twinKey]));
  });

  Object.keys(doppelgangers).forEach(doppelgangerKey => {
    if (doppelgangers[doppelgangerKey].length == 1) return;
    let details = [
      paletteTag,
      patternNames[doppelgangers[doppelgangerKey][0].fur],
      expressionNames[doppelgangers[doppelgangerKey][0].face],
      poseNames[doppelgangers[doppelgangerKey][0].pose]
    ];

    // Add CSV data
    doppelgangers[doppelgangerKey].forEach(cat => {
      csv.push(`${cat.id},"Doppleganger Twin","${details.join(',')}"`);
    });

    // Add HTML data
    let catCount = doppelgangers[doppelgangerKey].length;
    colorHtml.push(`<h2>Doppleganger Twins: ${details.join(', ')}</h2>`);
    colorHtml.push(`<p>Cats who, when rendered, all have a primary color of <tt>${colorCode}</tt> <span class="color-chip" style="background-color:${colorCode}">&nbsp;</span> and have the same fur pattern, facial expression and pose. A total of ${catCount} cats are in this set.</p>`);
    colorHtml.push(showCatList(doppelgangers[doppelgangerKey]));
  })

});

html.push('<div id="tooltip"></div>');
html.push('</body></html>');
colorHtml.push('<div id="tooltip"></div>');
colorHtml.push('</body></html>');
fs.writeFileSync('build/traits.csv', csv.join("\n"));
fs.writeFileSync('build/traits.html', html.join("\n"));
fs.writeFileSync('build/colors.html', colorHtml.join("\n"));
console.log('Done!');